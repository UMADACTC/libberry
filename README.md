# README #

### ¿Para qué es este repositorio? ###

* Este repositorio contiene el fichero base para desarrollar la librería "libberry" para las prácticas en ensamblador ARM con la Raspberry de la asignatura de Tecnología de Computadores de los Grados en Informática de la Universidad de Málaga

### ¿Que hacer con ellos? ###

* Descargarlos y programar las funciones que se indican en el guión de prácticas en el fichero **libberry.s**.
* Para probar los programas de ejemplo:
    - **blinkBerry.s** --> gcc blinkBerry.s libberry.s -lwiringPi -o blinkBerry
    - **soundBerry.s** --> gcc soundBerry.s libberry.s -lwiringPi -o soundBerry
* Ensamblar la librería una vez implementadas las funciones: 
    - **as libberry.s -o libberry.o**
* Para probar la función "setLeds", enlaza la librería con el fichero objeto pruLedsLibBerry.o proporcionado:
    - **gcc pruLedsLibBerry.o libberry.o -lwiringPi -o pruLedsLibBerry**
* Para probar la función "playNote", enlaza la librería con el fichero objeto pruSoundLibBerry.o proporcionado: 
    - **gcc pruSoundLibBerry.o libberry.o -lwiringPi -o pruSoundLibBerry**
* Para probar las dos funciones juntas reproduciendo música y encendiendo los leds a modo de ecualizador:
    - **gcc musicBerry.o libberry.s -lwiringPi -o musicBerry** 
    - **gcc thunderBerry.o lib berry.s -lwiringPi -o thunderBerry**